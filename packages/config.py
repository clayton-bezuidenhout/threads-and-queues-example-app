CONFIG = {
    'export_1': {
        'transform': 'JSON',
        'request_url': 'http://localhost:1880/records',
        'file_name_pattern': 'export1-{}.json'
    },
    'export_2': {
        'transform': 'JSON',
        'request_url': 'http://localhost:1880/records2',
        'file_name_pattern': 'export2-{}.json'
    }
}
# in all cases, the request url should return an array of objects.
