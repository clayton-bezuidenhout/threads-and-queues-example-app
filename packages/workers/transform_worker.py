import json
import logging
from copy import deepcopy
from queue import Queue
from time import sleep

from packages.models.ProcessTypes import ProcessType
from packages.models.QueueMessage import QueueMessage
from packages.models.TransformTypes import TransformType

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def transform_worker(transform_queue: Queue, writer_queue: Queue):
    """
    This function waits for data and transform it based on the transform property set in QueueMessage
    :param transform_queue : queue object reference
    :param writer_queue : queue object reference
    """
    log.info("Transform Thread started")

    while True:
        try:
            queue_msg: QueueMessage = transform_queue.get()

            if queue_msg:
                if queue_msg.process is ProcessType.EXPORT:

                    writer_msg = deepcopy(queue_msg)
                    # cleans out old data from the deepcopy

                    # Based on configuration of "transform" in config.py item
                    if queue_msg.transform is TransformType.JSON:
                        # convert data to json string
                        writer_msg.data = json.dumps(queue_msg.data)

                    if queue_msg.transform is TransformType.CSV:
                        # convert data to csv string
                        for key in queue_msg.data:
                            # can only join string values, so have to convert all objects in data
                            if not isinstance(queue_msg.data[key], str):
                                queue_msg.data[key] = str(queue_msg.data[key])
                        writer_msg.data = ";".join(queue_msg.data.values())

                    # send payload with converted data out to writer thread
                    log.info(f"Sending to write:{len(writer_msg.data.encode('utf-8'))} bytes")
                    writer_queue.put(writer_msg)

                else:
                    log.info(f'Transform Worker passing on {queue_msg.process}')
                    writer_queue.put(queue_msg)

            # release read on queue
            else:
                sleep(1)
            transform_queue.task_done()

        except Exception as e:
            log.info(f"An error has occurred in the transform worker.")
            log.error(e)
