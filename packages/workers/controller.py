"""
REST Controller
===============
Exposes endpoints to trigger export processes.
"""
import logging
import os

from copy import deepcopy
from flask import Flask, request, Response
from datetime import datetime
from queue import Queue

from packages.config import CONFIG
from packages.models.ProcessTypes import ProcessType
from packages.models.QueueMessage import QueueMessage
from packages.models.TransformTypes import TransformType

app = Flask(__name__)
TRIGGER_QUEUE: Queue = None

flask_log = logging.getLogger('werkzeug')
flask_log.setLevel(logging.ERROR)

# shim to get rid of prod warning. You can ignore during development.
os.environ['FLASK_ENV'] = 'prod'

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


@app.route('/export', methods=['GET'])
def trigger_export():
    try:
        # get export name from url parameters
        export_name = request.args.get('export_name', type=str)
        # create a string timestamp
        file_stamp = datetime.now().strftime("%Y_%m_%d-%H_%M_%S_.%f")
        # replace any {} value with a file name valid timestamp
        file_name = CONFIG[export_name]['file_name_pattern'].replace("{}", file_stamp)

        # construct start message
        start_msg = QueueMessage(
            process=ProcessType.SOP,
            name=export_name,
            file_name=file_name,
            transform=TransformType[CONFIG[export_name]['transform']]
        )

        # copy start message and change process type to EXPORT, add the request_url for the export to the payload.
        export_msg = deepcopy(start_msg)
        export_msg.process = ProcessType.EXPORT
        export_msg.request_url = CONFIG[export_name]['request_url']

        # copy start message and change process type to EOF
        end_msg = deepcopy(start_msg)
        end_msg.process = ProcessType.EOP

        # send messages in sequence start, export, end
        TRIGGER_QUEUE.put(start_msg)
        TRIGGER_QUEUE.put(export_msg)
        TRIGGER_QUEUE.put(end_msg)

        log.info(f'Triggered Export: {export_msg.name}')

    except ValueError as e:
        log.error(e)
        return Response(
            'request failed. Bad parameter received',
            status=400,
            mimetype='application/text'
        )
    except Exception as e:
        log.error(e)
        return Response(
            'request failed due to a server problem',
            status=500,
            mimetype='application/text'
        )

    return Response(
        f'{export_name} triggered. Output:\n {export_msg}',
        status=200,
        mimetype='application/text'
    )


def controller_worker(trigger_queue: Queue):
    """
    REST Server worker process.
    """
    global TRIGGER_QUEUE
    TRIGGER_QUEUE = trigger_queue
    log.info("Controller Thread started")
    app.run(host="0.0.0.0", debug=False, port=8800)
