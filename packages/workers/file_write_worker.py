import logging
import os
from queue import Queue

from packages.models.ProcessTypes import ProcessType
from packages.models.QueueMessage import QueueMessage
from packages.models.TransformTypes import TransformType

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def file_write_worker(writer_queue: Queue):
    """
    This function waits for data and writes it to file
    :param writer_queue : queue object reference
    """
    log.info("File Writer Thread started")

    writer_objects: dict = {}

    while True:
        try:
            queue_msg: QueueMessage = writer_queue.get()

            if queue_msg is not None:  # sanity check
                # Handles each record received from the rest request in the request_worker
                if queue_msg.process is ProcessType.EXPORT:

                    if queue_msg.transform is TransformType.JSON:
                        writer_objects[queue_msg.name].write(queue_msg.data.encode())
                        writer_objects[queue_msg.name].write(b",\n")

                    elif queue_msg.transform is TransformType.CSV:
                        # unicode_escape needed to keep \r and \n values from breaking the CSV
                        writer_objects[queue_msg.name].write(queue_msg.data.encode('unicode_escape'))
                        writer_objects[queue_msg.name].write(b"\n")

                # before the request gets made to retrieve data, this message will arrive here
                # and ready the file writer for the export objects it is about to receive
                elif queue_msg.process is ProcessType.SOP:
                    # open a writer object specific to the export
                    writer_objects[queue_msg.name] = open(queue_msg.file_name, 'wb')
                    log.info(f"opened file {queue_msg.file_name}")

                    # because we will be writing valid json objects, we need to decorate the file with array brackets
                    # at the start and end of the file
                    if queue_msg.transform is TransformType.JSON:
                        writer_objects[queue_msg.name].write(b"[\n")

                # Will be received when all data objects of an export has already been written to file
                elif queue_msg.process is ProcessType.EOP:

                    if queue_msg.transform is TransformType.JSON:
                        # because every line gets appended with a `,` comma in the export ProcessType, we need to remove
                        # the last character from the file and close of the bracket we opened in ProcessType.SOP
                        writer_objects[queue_msg.name].seek(writer_objects[queue_msg.name].tell() - 2, os.SEEK_SET)
                        writer_objects[queue_msg.name].truncate()
                        writer_objects[queue_msg.name].write(b"\n]\n")

                    # close file and remove file object from object holder
                    writer_objects[queue_msg.name].close()
                    del writer_objects[queue_msg.name]
                    log.info(f"closed file {queue_msg.file_name}")

            # release read on queue
            writer_queue.task_done()

        except Exception as e:
            log.error(f"An error has occured in the file write worker, {e}")
