import json
import logging
from copy import deepcopy
from queue import Queue
import requests

from packages.models.ProcessTypes import ProcessType
from packages.models.QueueMessage import QueueMessage

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def request_worker(trigger_queue: Queue, transform_queue: Queue):
    """
    This function makes a request to a rest endpoint and breaks up the data into queue messages per record
    :param trigger_queue : queue object reference
    :param transform_queue : queue object reference
    """
    log.info("Request Thread started")

    while True:
        try:
            queue_msg: QueueMessage = trigger_queue.get()

            if queue_msg:
                # We need to filter out the processes we don't want to handle. Like START and END of process. They
                # need to be forwarded down the process, so that the file writer thread can open and close a file.
                if queue_msg.process is ProcessType.EXPORT:

                    log.info(f'Starting Export {queue_msg.name}')
                    # get data from REST endpoint
                    response = requests.get(queue_msg.request_url)

                    if response.text is not []:
                        received_data = json.loads(response.text)
                        for item in received_data:
                            transform_msg = deepcopy(queue_msg)
                            transform_msg.data = item

                            # this data is not needed in any downstream threads
                            del transform_msg.request_url

                            # send one message per object received from the REST request
                            log.debug(f"Sending object to transform {transform_msg.data}")
                            transform_queue.put(transform_msg)

                else:
                    log.info(f'Request Worker passing on {queue_msg.process}')
                    transform_queue.put(queue_msg)

                # release read on queue
                trigger_queue.task_done()

        except Exception as e:
            log.error(f"An error has occured in the request worker, {e}")
