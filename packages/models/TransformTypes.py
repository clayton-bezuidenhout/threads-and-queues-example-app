from enum import Enum


class TransformType(Enum):
    """
    Transformer type definitions to use during the operation of the application.
    Applied in config
    """
    JSON = "JSON"
    CSV = "CSV"
