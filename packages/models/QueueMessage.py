from dataclasses import dataclass
from typing import Any

from packages.models.ProcessTypes import ProcessType
from packages.models.TransformTypes import TransformType


@dataclass
class QueueMessage:
    process: ProcessType
    transform: TransformType
    name: str
    file_name: str
    request_url: str = None
    data: Any = None
