from enum import Enum


class ProcessType(Enum):
    """
    Process type definitions to use during the operation of the application.
    """
    EXPORT = "EXPORT"
    SOP = "START_OF_PROCESS"
    EOP = "END_OF_PROCESS"
