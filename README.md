# README #

This apps configuration works as is if you spin up the node-red API localhost and import the 
`docker/example-app-flow.json` file in node red (sidebar > import).You can access 
the node-RED UI via `http://localhost:1880/`. 

To read up more about how node-RED works, you can visit `https://nodered.org/`.

### What this example app can do:
- Trigger a data export on API endpoint: `http://localhost:8800/export?export_name=export_1`
- Request data from an endpoint that was configured based on the export name. 
(Endpoint must return array of objects only)
- Transforms the return records into JSON or CSV , depending on config of export name
- writes to a file using a timestamped file name derived from config of export name 

### Run the script

Ensure the node-RED mock server works, then run the command in root of repository:

`python3 main_app.py` 

#### Note:
This is only a sample application, intended to showcase the core of a concept rather than 
a production ready implementation.
