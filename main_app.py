import logging
from queue import Queue
from threading import Thread
from time import sleep

from packages.workers.controller import controller_worker
from packages.workers.file_write_worker import file_write_worker
from packages.workers.request_worker import request_worker
from packages.workers.transform_worker import transform_worker

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def main() -> None:
    """
    Main application process. Manages threads and communication queues between them
    return: None
    """
    # Definitions
    q = {
        'trigger': Queue(),
        'transform': Queue(),
        'writer': Queue(),
    }

    threads = {
        # - Daemon setting True on all threads to ensure threads die if application exits.
        'controller': Thread(target=controller_worker, args=[q['trigger']], daemon=True),
        'request': Thread(target=request_worker, args=[q['trigger'], q['transform']], daemon=True),
        'transform': Thread(target=transform_worker, args=[q['transform'], q['writer']], daemon=True),
        'file_write': Thread(target=file_write_worker, args=[q['writer']], daemon=True),
    }

    # Start Threads
    for key in threads:
        threads[key].start()

    # Keep alive loop.Checks health of threads and exit if any thread dies.
    running = True
    while running:

        for key in threads:
            if not threads[key].is_alive():
                log.error(f"{key} Thread died. Exiting Application")
                running = False
                break

        log.debug('Thread health check complete')
        sleep(60)

    exit(1)  # Threads should not break, so we always exit with code 1


if __name__ == '__main__':
    log.info("Starting Application")
    main()
